import lifelib
import os
import csv

lt = None
def get_lifetree(memory=1000):
    global lt
    if not lt:
        lt = lifelib.load_rules("b3s23").lifetree(memory=memory)
    return lt

def memoize(f):
    memo = {}
    def helper(x):
        if x not in memo:            
            memo[x] = f(x)
        return memo[x]
    return helper

@memoize
def get_pattern(apgcode):
    return get_lifetree().pattern(apgcode)


def get_still_lifes_in_range(lifetree, n_cells_from, n_cells_to):
    for n_cells in range(n_cells_from, n_cells_to):
        for pat in get_still_lifes(lifetree, n_cells):
            yield pat

def get_still_lifes(lifetree, n_cells):
    apgcodes = get_still_lifes_apgcodes(n_cells)
    for apgcode in apgcodes:
        yield lifetree.pattern(apgcode) 

def get_still_lifes_apgcodes_in_range(n_cells_from, n_cells_to):
    for n_cells in range(n_cells_from, n_cells_to):
        for pat in get_still_lifes_apgcodes(n_cells):
            yield pat

def get_still_lifes_apgcodes(n_cells):
    still_lifes_filename = get_still_lifes_filename(n_cells)
    apgcodes = read_apgcodes(still_lifes_filename)
    return apgcodes

def get_still_lifes_filename(n_cells):
    return os.path.join("catagolue", f"xs{n_cells}.csv")

def read_apgcodes(still_lifes_filename):
    apgcodes = []
    with open(still_lifes_filename, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            apgcodes.append(row["apgcode"])
    return apgcodes
