def center(pattern):
    x0, y0, x1, y1 = bounding_box(pattern)
    xc, yc = x0 + (x1 - x0)//2, y0 + (y1 - y0)//2
    return pattern(-xc, -yc)

def bounding_box(pattern):
    coords = pattern.coords()
    x0 = coords[:,0].min()
    x1 = coords[:,0].max() + 1
    y0 = coords[:,1].min()
    y1 = coords[:,1].max() + 1
    return x0, y0, x1, y1

def perimeter(pattern):
    return pattern(0, 1) + pattern(1, 0) + pattern(0, -1) + pattern(-1, 0) +\
            pattern(1, 1) + pattern(-1, -1) + pattern(1, -1) + pattern(-1, 1) \
            - pattern

def cells(pattern):
    for i,j in pattern.coords():
        yield single_cell_pattern(pattern.owner, i, j)

def single_cell_pattern(lt, i, j):
    pattern = lt.pattern()
    pattern[i, j] = 1
    return pattern

def two_cells(pattern):
    for i,j in pattern.coords():
        if pattern[i, j + 1] == 1:
            yield double_cells_pattern(pattern.owner, i, j, i, j+1)
        elif pattern[i + 1, j] == 1:
            yield double_cells_pattern(pattern.owner, i, j, i+1, j)
        elif pattern[i + 1, j - 1] == 1:
            yield double_cells_pattern(pattern.owner, i, j, i + 1, j - 1)
        elif pattern[i + 1, j + 1] == 1:
            yield double_cells_pattern(pattern.owner, i, j, i + 1, j + 1)

def double_cells_pattern(lt, i, j, k, l):
    pattern = lt.pattern()
    assert i!=k or j!=l, f"{i}, {j}, {k}, {l}" 
    pattern[i, j] = 1
    pattern[k, l] = 1
    return pattern
