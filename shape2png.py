import lifelib
import argparse
import numpy as np
import matplotlib.pyplot as plt
import os.path

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("shape")
    ap.add_argument('--save')
    args = ap.parse_args()

    shape2png(args.shape, args.save)

def shape2png(shape, save=None):
    lt = lifelib.load_rules("b3s23").lifetree()
    shape = lt.pattern(shape)
    x0, y0, x1, y1 = shape.bounding_box
    X = np.zeros((x1-x0+2, y1-y0+2))
    coords = shape.coords()
    X[coords[:,0]+1, coords[:,1]+1] = 1
    plt.imshow(X, cmap=plt.get_cmap("binary"), interpolation='nearest')
    # Gridlines based on minor ticks
    ax = plt.gca()
    ax.set_xticks(np.arange(-.5, X.shape[0], 1), minor=True);
    ax.set_yticks(np.arange(-.5, X.shape[1], 1), minor=True);
    ax.get_xaxis().set_visible(False)
    ax.get_yaxis().set_visible(False)
    ax.grid(which='minor', color='w', linestyle='-', linewidth=2)
    if save:
        plt.savefig(save, bbox_inches='tight')
    else:
        plt.savefig(os.path.join('img', shape + '.png'))

if __name__ == '__main__':
    main()
