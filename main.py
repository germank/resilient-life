import argparse
import loader
import perturbation
import resilience
import measure
import pandas as pd
import itertools
import tqdm

perturbations = {'add_one_cell_perimeter': perturbation.add_one_cell_perimeter,
        'subtract_one_cell': perturbation.subtract_one_cell, 
        'add_two_cells_perimeter': perturbation.add_two_cells_perimeter,
        'subtract_two_cells': perturbation.subtract_two_cells}
measures = {'equality': measure.equality, 'inclusion': measure.inclusion,
        'cosine': measure.cosine}

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('--start', type=int, default=4)
    ap.add_argument('--end', type=int, default=10)
    ap.add_argument('--steps', type=int, default=100)
    ap.add_argument('--save', default="results/still_lifes_{perturbations}_{start}_{end}.csv")
    ap.add_argument('--memory', type=int, default=1000)
    ap.add_argument('-p', '--perturbation', action='append', default=[], required=True)
    args = ap.parse_args()

    lt = loader.get_lifetree(memory=args.memory)
    print(f'Exploring patterns of size {args.start} to {args.end} using {args.perturbation} perturbations')
    if 'all' in args.perturbation:
        selected_perturbations = perturbations
    else:
        selected_perturbations = {k: perturbations[k] for k in args.perturbation}

    still_lifes_apg = loader.get_still_lifes_apgcodes_in_range(
            args.start, args.end)
    results = evaluate_resilience_still_lifes(lt, measures, selected_perturbations, still_lifes_apg, args.steps)
    save_results(args, results)

def save_results(args, results):
    df = pd.DataFrame(results)
    df.to_csv(args.save.format(start=args.start, end=args.end, perturbations="-".join(args.perturbation)))

def evaluate_resilience_still_lifes(lt, measures, perturbations, still_lifes_apg, steps):
    results = []
    for sl_apg in tqdm.tqdm(list(still_lifes_apg)):  #shows a progress bar
        sl = lt.pattern(sl_apg)
        for perturbation in perturbations:
            sl_resilience_measures = resilience.get_mean_resilience_still_life(sl,
                    perturbations[perturbation], measures, steps)
            resilience_steps=resilience.count_res_steps(sl, perturbations[perturbation], measures, steps)
            disappear_steps=resilience.count_dis_steps(sl, perturbations[perturbation], measures, steps)
            for measure in sl_resilience_measures:
                results.append({'measure': measure, 
                    'perturbation': perturbation,
                    'pattern': sl.apgcode,
                    'resilience': sl_resilience_measures[measure], 
                    'population': sl.population,
                    'res_steps': resilience_steps[measure],
                    'dis_steps': disappear_steps[measure]
                })
    return results


main()
