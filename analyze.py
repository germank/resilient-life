import pandas as pd
import loader
from tqdm import tqdm
import measure
import pattern
import connected_components

MIN_SHAPE_SIZE=4
MAX_SHAPE_SIZE=42

def main():
    df = shapes_dataframe()
    tqdm.pandas()
    add_symmetry(df)
    add_density(df)
    add_connected_components(df)
    df.to_csv('results/pattern_features.csv')

def shapes_dataframe():
    shapes = list(loader.get_still_lifes_apgcodes_in_range(MIN_SHAPE_SIZE, MAX_SHAPE_SIZE+1))
    return pd.DataFrame ({'pattern': shapes})

def add_density(df):
    df['density'] = df['pattern'].progress_apply(get_density)

def add_connected_components(df):
    df['connected_components'] = df['pattern'].progress_apply(
            get_connected_components)

def get_connected_components(apgcode):
    pat = loader.get_pattern(apgcode)
    return connected_components.get_number_subcomponents(pat)

def get_density(apgcode):
    pat = loader.get_pattern(apgcode)
    return pat.population / get_area(pat)

def get_area(pat):
    i1, j1, i2, j2 = pat.bounding_box
    return (i2-i1)*(j2-j1)

def add_symmetry(df):
    transformations = ["flip", "rot180", "transpose", "flip_x", "flip_y",
                        "rot90", "rot270", "swap_xy", "swap_xy_flip", "rcw",
                        "rccw"]
    measures = {'equality': measure.equality, 'cosine': measure.cosine,
        'inclusion': measure.inclusion}
    
    for transformation in transformations:
        for measure_name, measure_f in measures.items():
            key = f"{transformation}_{measure_name}"
            df[key] = df['pattern'].progress_apply(
                    lambda apgcode: measure_after_transformation(
                        apgcode, measure_f, transformation))

def measure_after_transformation(apgcode, measure_f, transformation):
    pat = loader.get_pattern(apgcode)
    return measure_f(pattern.center(pat), 
            pattern.center(pat.transform(transformation)))

main()
