#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import os.path
from scipy.stats import pearsonr

STILL_LIFES_FILENAME = 'results/still_lifes_all_4_43.csv'
FEATURES_FILENAME = 'results/pattern_features.csv'
CONNECTED_COMPONENTS_FILENAME = 'results/pattern_connected_components.csv'
MEASURES = ['equality', 'inclusion', 'cosine']
PERTURBATIONS = ['add_one_cell_perimeter', 'subtract_one_cell']# 'add_two_cells_perimeter', 'subtract_two_cells']
pd.options.display.float_format = '{:,.2f}'.format

def main():
    df_still_lifes = pd.read_csv(STILL_LIFES_FILENAME)
    df_features = pd.read_csv(FEATURES_FILENAME)
    df_connected_components = pd.read_csv(CONNECTED_COMPONENTS_FILENAME)
    del df_still_lifes['res_steps']
    del df_still_lifes['dis_steps']

    df_merged = pd.merge(df_connected_components, df_features, on='pattern')
    df_merged = pd.merge(df_still_lifes, df_merged, on='pattern')
    df_merged = df_merged.loc[:, ~df_merged.columns.str.contains('^Unnamed')]
    print(df_merged.corr()['population'])
    for measure in MEASURES:
        for perturbation in PERTURBATIONS:
            df_only_measure_perturbation = get_only_measure_perturbation(
                    df_merged, measure, perturbation)
            df_correlations = df_only_measure_perturbation.corr('pearson')
            df_pvalues = calculate_pvalues(df_only_measure_perturbation)#.corr('pearson')
            ds_resilience_corr = df_correlations['resilience']
            ds_resilience_pvalues = df_pvalues['resilience']
            largest = ds_resilience_corr.drop_duplicates('first').abs().nlargest(10).index
            print(measure, perturbation)
            print(ds_resilience_corr[largest].iloc[1:])
            print(ds_resilience_pvalues[largest].iloc[1:])

def calculate_pvalues(df):
    df = df.dropna()._get_numeric_data()
    dfcols = pd.DataFrame(columns=df.columns)
    pvalues = dfcols.transpose().join(dfcols, how='outer')
    for r in df.columns:
        for c in df.columns:
            pvalues[r][c] = pearsonr(df[r], df[c])[1]
    return pvalues


def get_only_measure_perturbation(df, measure, perturbation):
    return df[(df['measure'] == measure) & (df['perturbation'] == perturbation)]


if __name__ == '__main__':
    main()
