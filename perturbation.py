import pattern

def add_one_cell_perimeter(pat):
    pat_perimeter = pattern.perimeter(pat)
    for cell_in_perimeter in pattern.cells(pat_perimeter):
        yield pat + cell_in_perimeter

def subtract_one_cell(pat):
    for cell in pattern.cells(pat):
        yield pat - cell

def add_two_cells_perimeter(pat):
    pat_perimeter = pattern.perimeter(pat)
    for cell_in_perimeter in pattern.two_cells(pat_perimeter):
        yield pat + cell_in_perimeter

def subtract_two_cells(pat):
    for cell in pattern.two_cells(pat):
        yield pat - cell