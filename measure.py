import math

def equality(pat1, pat2):
    return int(pat1 == pat2)

def inclusion(original, perturbed):
    return int(original <= perturbed)

def cosine(original, perturbed):
    if perturbed.population == 0:
        return 0
    intersection = original & perturbed
    return intersection.population /  (math.sqrt(original.population) * 
            math.sqrt(perturbed.population))
