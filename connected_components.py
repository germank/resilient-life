import loader
from collections import defaultdict
import tqdm
import numpy as np
import networkx as nx
import pickle

def main():
    lt = loader.get_lifetree()
    subcomponents = defaultdict(list)
    all_apgcodes = list(loader.get_still_lifes_apgcodes_in_range(4, 43))
    for apgcode in tqdm.tqdm(all_apgcodes):
        pat = lt.pattern(apgcode)
        pats = get_subcomponents(lt, pat)
        try:
            if len(pats) > 1 and all(p[1] == p for p in pats):
                subcomponents[pat.apgcode] = [p.apgcode for p in pats]
                print(subcomponents[pat.apgcode])
        except KeyError:
            pass
    print (subcomponents)
    pickle.dump(subcomponents, open('subcomponents.pkl', 'wb'))

def get_subcomponents(lt, pat):
    graph = pattern2graph(pat)
    connected_components = nx.connected_components(graph)
    subcomponents = [nodes2pattern(lt, sc) for sc in connected_components]
    return subcomponents

def get_number_subcomponents(pat):
    graph = pattern2graph(pat)
    connected_components = nx.connected_components(graph)
    return sum(1 for c in connected_components)

def pattern2graph(pat):
    graph = nx.Graph()
    coords = set(tuple(c) for c in pat.coords())
    for c in coords:
        graph.add_node(c)

    for c1 in coords:
        for c2 in coords:
            if is_neighbour(c1, c2):
                graph.add_edge(c1, c2)
    return graph

def nodes2pattern(lt, component):
    pat = lt.pattern()
    for c in component:
        pat[c[0], c[1]] = 1 
    return pat

def is_neighbour(c1, c2):
    dx, dy = np.abs(np.array(c2) - np.array(c1))
    return dx <= 1 and dy <= 1


if __name__ == '__main__':
    main()
