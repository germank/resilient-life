import pattern

def get_mean_resilience_still_life(pat, perturbation_f, similarity_measures, n_steps=100):
    mean_resiliences_by_measure = {measure: 0 for measure in similarity_measures}
    for i, perturbed_pat in enumerate(perturbation_f(pat)):
        perturbed_pat = perturbed_pat[n_steps]
        for measure, measure_function in similarity_measures.items():
            mean_resiliences_by_measure[measure] += measure_function(pat, perturbed_pat)
    for measure in similarity_measures:
        mean_resiliences_by_measure[measure] /= (i + 1)
    return mean_resiliences_by_measure

def count_res_steps(pat, perturbation_f, similarity_measures, n_steps=100):
    recorded_res_steps = {measure: 0 for measure in similarity_measures}
    for i, perturbed_pat in enumerate(perturbation_f(pat)):
        #perturbed_pat = perturbed_pat[n_steps]
        for measure, measure_function in similarity_measures.items():
            recorded_res_steps[measure] = n_steps
            for tempt_step in range (n_steps):
                if (tempt_step !=0 ) and (perturbed_pat[tempt_step].population != 0):
                    if (measure_function(pat, perturbed_pat[tempt_step]) > 0) and (measure_function(pat, perturbed_pat[tempt_step]) == measure_function(pat, perturbed_pat[tempt_step-1])):
                        recorded_res_steps[measure] = tempt_step
                        break
    return recorded_res_steps

def count_dis_steps(pat, perturbation_f, similarity_measures, n_steps=100):
    recorded_dis_steps = {measure: 0 for measure in similarity_measures}
    for i, perturbed_pat in enumerate(perturbation_f(pat)):
        #perturbed_pat = perturbed_pat[n_steps]
        for measure, measure_function in similarity_measures.items():
            recorded_dis_steps[measure] = n_steps
            for tempt_step in range (n_steps):
                if (tempt_step !=0 ) and (perturbed_pat[tempt_step].population == 0):
                    if (measure_function(pat, perturbed_pat[tempt_step]) == measure_function(pat, perturbed_pat[tempt_step-1])):
                        recorded_dis_steps[measure] = tempt_step
                        break
    return recorded_dis_steps
