import unittest
import lifelib
import pattern

class TestPattern(unittest.TestCase):
    def setUp(self):
        self.lt = lifelib.load_rules("b3s23").lifetree()

    def test_perimeter(self):
        cell_pattern = self.one_cell_pattern()
        pattern_perimeter = pattern.perimeter(cell_pattern)
        for i in range(-1, 1+1):
            for j in range(-1, 1+1):
                if i == j == 0:
                    self.assertEqual(pattern_perimeter[i, j], 0)
                else:
                    self.assertEqual(pattern_perimeter[i, j], 1)

    def one_cell_pattern(self):
        cell_pattern = self.lt.pattern()
        cell_pattern[0, 0] = 1
        return cell_pattern

    def test_single_cells(self):
        for pat in self.blinker_patterns():
            cell_patterns = list(pattern.cells(pat))
            self.assertNoRepetitions(cell_patterns)
            self.assertEqual(len(cell_patterns), pat.population)
            self.assertPatternsPopulationEquals(cell_patterns, 1)

    def test_two_cells_on_blinker(self):
        for pat in self.blinker_patterns():
            two_cells_combinations = list(pattern.two_cells(pat))
            self.assertNoRepetitions(two_cells_combinations)
            self.assertEqual(len(two_cells_combinations), pat.population-1)
            self.assertPatternsPopulationEquals(two_cells_combinations, 2)

    def test_two_cells_on_square(self):
        cell = self.one_cell_pattern()
        square = pattern.perimeter(cell)
        two_cells_combinations = list(pattern.two_cells(square))
        self.assertNoRepetitions(two_cells_combinations)
        self.assertEqual(len(two_cells_combinations), square.population-1)
        self.assertPatternsPopulationEquals(two_cells_combinations, 2)

    def assertPatternsPopulationEquals(self, patterns, k):
        patterns_coords = self.patterns_to_coords(patterns)
        for coords in patterns_coords:
            self.assertEqual(len(set(coords)), k)
            self.assertEqual(len(coords), k)

    def test_two_cells_repetitions(self):
        for pat in self.blinker_patterns():
            self.assertNoRepetitions(pattern.two_cells(pat))

    def assertNoRepetitions(self, patterns):
        patterns_coords = self.patterns_to_coords(patterns)
        self.assertEqual(len(patterns_coords), len(set(patterns_coords))) 

    def patterns_to_coords(self, patterns):
        patterns_coords = []
        for cell_pattern in patterns:
            coords = tuple(tuple(c) for c in cell_pattern.coords())
            patterns_coords.append(coords)
        return patterns_coords

    def blinker_patterns(self):
        blinker_pattern = self.blinker_pattern()
        blinker_pattern_rot = blinker_pattern[1]
        assert blinker_pattern != blinker_pattern_rot
        return [blinker_pattern, blinker_pattern_rot]

    def blinker_pattern(self):
        blinker_pattern = self.lt.pattern()
        for i in range(3):
            blinker_pattern[i, 0] = 1
        return blinker_pattern

    def one_cell_pattern(self):
        cell_pattern = self.lt.pattern()
        cell_pattern[0, 0] = 1
        return cell_pattern

    def test_center(self):
        blinker = self.blinker_pattern()
        blinker = blinker(-2, 0)
        centered_blinker = pattern.center(blinker)
        for i in range(-1, 2):
            self.assertEqual(centered_blinker[i, 0], 1)
        self.assertEqual(centered_blinker[-2, 0], 0)
        self.assertEqual(centered_blinker[2, 0], 0)

    def test_flip(self):
        blinker = self.blinker_pattern()
        mirror_blinker = blinker.transform("flip")
        centered_blinker = pattern.center(blinker) 
        centered_mirror_blinker = pattern.center(mirror_blinker)
        self.assertTrue(centered_blinker == centered_mirror_blinker)

    def test_bounding_box(self):
        blinker = self.blinker_pattern()
        blinker = blinker(1, 1)
        x0, y0, x1, y1 = pattern.bounding_box(blinker)
        self.assertEqual(x0, 1)
        self.assertEqual(y0, 1)
        self.assertEqual(x1, 4)
        self.assertEqual(y1, 2)



        
