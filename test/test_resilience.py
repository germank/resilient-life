import unittest
import lifelib
import measure
import resilience

class TestResilience(unittest.TestCase):
    def setUp(self):
        self.lt = lifelib.load_rules("b3s23").lifetree()

    def test_mean_resilience_null_perturbation(self):
        block = self.block_pattern()
        block_resilience = resilience.get_mean_resilience_still_life(
                block, self.null_perturbations, {'equality': measure.equality})
        self.assertEqual(block_resilience['equality'], 1)

    def block_pattern(self):
        block_pattern = self.lt.pattern()
        block_pattern[0:2, 0:2] = 1
        return block_pattern

    def null_perturbations(self, pat):
        yield self.lt.pattern(pat.rle_string())
        yield self.lt.pattern(pat.rle_string())
        yield self.lt.pattern(pat.rle_string())
        yield self.lt.pattern(pat.rle_string())

