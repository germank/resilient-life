import unittest
import lifelib
import perturbation

class TestPerturbation(unittest.TestCase):
    def setUp(self):
        self.lt = lifelib.load_rules("b3s23").lifetree()

    def test_single_cell_perimeter_perturbation(self):
        cell_pattern = self.one_cell_pattern()
        perturbations = perturbation.add_one_cell_perimeter(cell_pattern)
        for perturbed_pat in perturbations:
            sum_cells = 0
            for i in range(-1, 1+1):
                for j in range(-1, 1+1):
                    sum_cells += perturbed_pat[i, j]
            self.assertEqual(sum_cells, 2)

    def test_two_cell_perimeter_perturbation(self):
        cell_pattern = self.one_cell_pattern()
        perturbations = list(perturbation.add_two_cells_perimeter(cell_pattern))
        for perturbed_pat in perturbations:
            self.assertEqual(perturbed_pat.population, 3)
        self.assertEqual(len(perturbations), 7)

    def test_two_cell_perimeter_two_separated(self):
        pat = self.two_separated_cells_pattern()
        perturbations = list(perturbation.add_two_cells_perimeter(pat))
        self.assertEqual(len(perturbations), 12)

    def test_two_cell_perimeter_four_separated(self):
        pat = self.four_separated_cells_pattern()
        perturbations = list(perturbation.add_two_cells_perimeter(pat))
        self.assertEqual(len(perturbations), 20)
    
    def four_separated_cells_pattern(self):
        pat = self.lt.pattern()
        pat[0, 0] = 1
        pat[0, 2] = 1
        pat[2, 0] = 1
        pat[2, 2] = 1
        return pat

    def two_separated_cells_pattern(self):
        pat = self.lt.pattern()
        pat[0, 0] = 1
        pat[0, 2] = 1
        return pat

    def one_cell_pattern(self):
        cell_pattern = self.lt.pattern()
        cell_pattern[0, 0] = 1
        return cell_pattern
