import unittest
import lifelib
import measure
import math

class TestMeasure(unittest.TestCase):
    def setUp(self):
        self.lt = lifelib.load_rules("b3s23").lifetree()

    def test_cosine(self):
        p1 = self.one_cell_pattern()
        p2 = p1 + p1(1, 0)
        self.assertAlmostEqual(measure.cosine(p1, p2), math.sqrt(0.5))
        self.assertAlmostEqual(measure.cosine(p2, p1), math.sqrt(0.5))
        self.assertAlmostEqual(measure.cosine(p2, p2), 1)
        self.assertAlmostEqual(measure.cosine(p1, p1), 1)
        self.assertAlmostEqual(measure.cosine(p1, p1(1,0)), 0)
        px = self.lt.pattern("xs6_696")
        p0 = self.lt.pattern()
        self.assertAlmostEqual(measure.cosine(px, px), 1)
        self.assertAlmostEqual(measure.cosine(px, p0), 0)

    def test_inclusion(self):
        p1 = self.one_cell_pattern()
        p2 = p1 + p1(1, 0)
        self.assertEqual(measure.inclusion(p1, p2), 1)
        self.assertEqual(measure.inclusion(p2, p1), 0)

    def one_cell_pattern(self):
        cell_pattern = self.lt.pattern()
        cell_pattern[0, 0] = 1
        return cell_pattern
